export class Stagiaire {

    constructor(
        public id: number,
        public lastname: string,
        public firstname,
        public actif: boolean,
        public age: number
    )
    {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.actif = actif;
        this.age = age;
    };
}
