import { Component, OnInit, Input } from '@angular/core';
import { Stagiaire } from  '../stagiaire';


@Component({
  selector: 'app-aston',
  templateUrl: './aston.component.html',
  styleUrls: ['./aston.component.scss']
})
export class AstonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() firstname: string;
  stagiaire = ['Philippe', 'Emma', 'Alan', 'Belhassen'];

  tabs = [
    new Stagiaire(1, 'Belhassen', 'Bouchoucha', true, 21),
    new Stagiaire(2, 'Thomas', 'Shelby', true, 30),
    new Stagiaire(3, 'Arthur', 'Shelby', true, 35)
  ];

}
