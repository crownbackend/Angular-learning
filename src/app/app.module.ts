import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AstonComponent } from './aston/aston.component';
import { StagiaireComponent } from './stagiaire/stagiaire.component';

@NgModule({
  declarations: [
    AppComponent,
    AstonComponent,
    StagiaireComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
